:set number
:set relativenumber
:set autoindent
:set tabstop=4
:set shiftwidth=4
:set smarttab
:set softtabstop=4
:set mouse=a

" ###KEY###
" imap <F2> <Esc> :CocCommand markdown-preview-enhanced.openPreview <CR>i
map <F2> :CocCommand markdown-preview-enhanced.openPreview <CR>

" ###SOURCE###
source $HOME/.config/nvim/plug.vim
source $HOME/.config/nvim/plug_set.vim
